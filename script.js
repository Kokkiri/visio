// only for development

db.list.remove({})
db.users.remove({})

db.users.insert({
  _id : "1",
  createdAt : new Date(),
  services : {
    password : {
      bcrypt : "$2a$10$1MaU5IbEknr6VvCTwFptQOMQnHK/xzryMFnof58kJoRMdMmmCtCAe"
    },
  },
  username : "ede"
});
db.users.insert({
  _id : "2",
  createdAt : new Date(),
  services : {
    password : {
      bcrypt : "$2a$10$JyNs/bGiqnny3a0dqDuFz.tnB9czzHoJHNUhW3uwL19z7rRrlaYFe"
    },
  },
  username : "momo"
});
db.users.insert({
  _id : "3",
  createdAt : new Date(),
  services : {
    password : {
      bcrypt : "$2a$10$w91o/mi5lRyuPdDKSUaZZOdcSXw1NfLRXY4LV7WajsqsVs5.OAw2u"
    },
  },
  username : "eole"
});
