# description:

visio application that allow condidates and jury to meet together.

## ONLY FOR DEVELOPMENT:

  first run:

    npm install

  to install node modules.

  Next run:

    ./initUsers.sh

  to seed users collection in data-base.

  there is three users:

    name: ede
    pwd: ede

    name: momo
    pwd: momo

    name: eole
    pwd: eole

## licence

This project is licensed under the MIT License
