import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Paper from '@material-ui/core/Paper'

import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'

import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormControl from '@material-ui/core/FormControl'


class FormModal extends React.Component {

  render() {
    const { title, close, isOpenModal, classes, linkToCandidatesList, searchInput, handleSearchBar, filter } = this.props
    const listNoms = () => {
      const list = filter().map(el => {
        return (
          <FormControlLabel
            key={el}
            value={el}
            control={<Radio color="primary" />}
            label={el}
          />
        )
      })
      return list
    }
    return (
      <div>
        <Dialog
          fullScreen={true}
          open={isOpenModal}
          onClose={close}
          aria-labelledby="form-dialog-title"

        >
          <Paper
            className={classes.paper}
          >
            <DialogTitle
              id="form-dialog-title"
              className={classes.color}
            >
              {`Liste des ${title.toLowerCase()}`}
            </DialogTitle>

            <TextField
              className={classes.space}
              placeholder="Recherche"
              onChange={handleSearchBar}
              value={searchInput}
            />

            <DialogContent>
              <FormControl component="fieldset" className={classes.space}>
                <RadioGroup
                  aria-label=""
                  name=""
                  className={classes.group}
                  onChange={this.handleChange}
                >
                  {listNoms()}
                </RadioGroup>
              </FormControl>
            </DialogContent>

            <DialogActions
              className={classes.buttonleft}
            >
              <Button
                onClick={close}
                color="secondary"
                variant="outlined"
              >
                Annuler
              </Button>
              {linkToCandidatesList}
            </DialogActions>

          </Paper>
        </Dialog>
      </div>
    );
  }

  handleChange = (e) => {
    const { getValue, setDisable } = this.props
    getValue(e.target.value)
    setDisable()
  }

}

const styles = theme => ({
  space: {
    margin: theme.spacing.unit * 3,
  },
  group: {
    margin: `${theme.spacing.unit}px 0`,
  },
  color: {
    background: "#99ddff",
  },
  paper: {
    margin: theme.spacing.unit * 3
  },
  buttonleft: {
    display: 'flex',
    justifyContent: 'left',
  }
});

FormModal.propTypes = {
  classes: PropTypes.object.isRequired,
};

const styledFormModal = withStyles(styles)(FormModal);
export { styledFormModal as FormModal }
