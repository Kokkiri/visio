import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { withStyles, createMuiTheme } from '@material-ui/core/styles'

import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'

import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import Slider from 'rc-slider'
import 'rc-slider/assets/index.css'

const createSliderWithTooltip = Slider.createSliderWithTooltip
const Range = createSliderWithTooltip(Slider.Range)


class CandidatListComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      redirect: false,
      disable: true,
      searchInput: "",
      length: 0,
      visible: false,
    }
  }

  setRedirect = () => {
    const { clearList } = this.props
    this.setState({redirect: true})
    clearList()
  }

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to='/' />
    }
  }

  handleSearchBar = (e) => {
    let userInput = e.target.value
    this.setState({searchInput: userInput})
  }

  // Itération sur les dates de passage pour l'affichage du range-slider
  dateTipFormatter = (value) => {
    console.log("TOTO", value)
    const { candidatList } = this.props
    const date = []
    candidatList.sort((a,b) => {
      return a.date > b.date
    })
    candidatList.map(el =>
      date.push(el.date.slice(0, 10))
    )
    // Suppression des doublons
    let uniq = a => [...new Set(a)]
    uniq(date)
    console.log("azer", uniq(date)[value]);
    return [uniq(date)[value], uniq(date).length]
  }

  // To set date-selection-bar visible
  handleVisible = () => {
    this.setState({visible: !this.state.visible})
  }

  frout = (value) => {
    console.log("FROUT", value)
  }

  render() {
    const { cellTitle3, candidatList, classes, linkToVideoConf, filter } = this.props
    const { disable, searchInput } = this.state
    const dateRangeLength = this.dateTipFormatter(value)[1]
    const eachCandidat = () => {
      if (candidatList.length === 0) {
        return <Redirect to='/' />
      } else {
        const list = filter(searchInput).map((el, i) => {
          let tirdColumn = ""
          if (cellTitle3 === "centres d'accueil du candidat") {
            tirdColumn = el.c_examen
          } else {
            tirdColumn = el.jury
          }
          let color = ""
          if (el.color === true) {
            color = classes.secondary
          } else {
            color = classes.primary
          }
          return (
            <TableRow
              key={el.candidat}
              hover={true}
            >
              <TableCell>
                {linkToVideoConf(el, color)}
              </TableCell>
              <TableCell>{el.date.slice(0, 10)}</TableCell>
              <TableCell>{el.heure}</TableCell>
              <TableCell>{el.candidat}</TableCell>
              <TableCell>{tirdColumn}</TableCell>
            </TableRow>
          )
        })
        return list
      }
    }
    return (
      <Paper className={classes.root}>
        {this.renderRedirect()}
        <div className={classes.color}>
          <p className={classes.p}>Liste des candidats</p>
        </div>
          <TextField
            className={classes.space}
            placeholder="Recherche"
            onChange={this.handleSearchBar}
            value={searchInput}
          />

          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
              onClick={this.handleVisible}
            >
            <p>Sélection des dates</p>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Range
                allowCross={false}
                min={0}
                onAfterChange={this.frout}
                max={dateRangeLength - 1}
                defaultValue={[0, dateRangeLength - 1]}
                tipProps={
                  {
                    visible: this.state.visible
                  }
                }
                tipFormatter={value => this.dateTipFormatter(value)[0]}
              />
            </ExpansionPanelDetails>
          </ExpansionPanel>

          <Table>
            <TableHead>
              <TableRow className={classes.cell}>
                <TableCell></TableCell>
                <TableCell>dates</TableCell>
                <TableCell>horaires</TableCell>
                <TableCell>candidats</TableCell>
                <TableCell>{cellTitle3}</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {eachCandidat()}
            </TableBody>
          </Table>
        <div className={classes.buttondiv} >
          <Button
            className={classes.button}
            onClick={this.setRedirect}
            color="secondary"
            variant="outlined"
          >Retour</Button>

        </div>
      </Paper>
    )
  }
}


const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column'
  },
  cell: {
    textTransform: "uppercase"
  },
  color: {
    background: "#99ddff",
    borderRadius: "3px 3px 0 0"
  },
  rangeSlide: {
    margin: '20px 50px',
  },
  rangeSlideButton: {
    marginBottom: '50px',
    marginLeft: theme.spacing.unit * 3,
    color: '#A5A5A5',
  },
  space: {
    margin: theme.spacing.unit * 3
  },
  p: {
    margin: theme.spacing.unit * 3,
    fontSize: "120%"
  },
  buttondiv: {
    width: "300px",
    display: 'flex',
    margin: `${theme.spacing.unit}px 0`
  },
  button: {
    margin: "0 3% 0 3%"
  },
  primary: {
    color: theme.palette.primary.main,
    borderColor: theme.palette.primary.main
  },
  secondary: {
    color: theme.palette.secondary.purple,
    borderColor: theme.palette.secondary.purple
  }
});

CandidatListComponent.propTypes = {
  classes: PropTypes.object.isRequired,
};

const styledCandidatListComponent = withStyles(styles)(CandidatListComponent);
export { styledCandidatListComponent as CandidatListComponent }
