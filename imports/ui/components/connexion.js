import React, { Component } from 'react'
import { withRouter, Redirect } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'

import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'

class Connexion extends Component {

  api = () => {
    const { history, nomCandidat, jury } = this.props
    let defaultLocalDisplayName
    if (history.location.pathname === "/centredexamen/connexion") {
      defaultLocalDisplayName = nomCandidat
    } else if (history.location.pathname === "/jury/connexion") {
      defaultLocalDisplayName = jury
    }
    const { roomName } = this.props
    const domain = Meteor.settings.public.domain
    const options = {
      roomName: roomName,
      noSLL: false,
      width: window.screen.width,
      height: window.screen.height - 160,
      parentNode: document.querySelector('#meet'),
      configOverwrite: {
        defaultLanguage: 'fr',
      },
      interfaceConfigOverwrite:{
        // filmStripOnly: true,
        DEFAULT_LOCAL_DISPLAY_NAME: '',
        TOOLBAR_BUTTONS: [],
      }
    }
    const api = new JitsiMeetExternalAPI(domain, options)
    api.executeCommands({
      displayName: defaultLocalDisplayName,
    });
    return api
  }

  componentDidMount() {
    const { history } = this.props
    let api = this.api()
    api.removeEventListeners(["incomingMessage", "outgoingMessageListener"])
    api.addEventListener("readyToClose", function() {
      api.dispose()
      history.goBack()
    });
  }

  goBack = () => {
    const { history } = this.props
    this.api().dispose()
    history.goBack()
  }

  redirect = () => {
    if (this.props.nomCandidat === "") {
      return <Redirect to='/' />
    }
  }

  openFullscreen = () => {
    var elem = document.getElementById("meet");
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
      elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
      elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
      elem.msRequestFullscreen();
    }
  }

  render() {
    const { classes, nomCandidat, cExamen, jury } = this.props
    return(
      <div className={classes.root}>
        {this.redirect()}

        <Table className={classes.table}>
          <TableBody>
            <TableRow>
              <TableCell>{nomCandidat}</TableCell>
              <TableCell>jury de {jury}</TableCell>
              <TableCell>centre d'examen de {cExamen}</TableCell>
              <TableCell><Button
                className={classes.space}
                onClick={this.goBack}
                color="secondary"
                variant="outlined"
                >Deconnexion</Button></TableCell>
            </TableRow>
          </TableBody>
        </Table>

        <div id="meet"></div>
        <div>
        </div>
      </div>
    )
  }
}

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: "column",
    alignItems: 'center',
    justifyContent: 'center',
  },
  cell: {
    textTransform: "uppercase"
  },
  space: {
    margin: theme.spacing.unit * 2
  },
  paper: {
    display: 'flex',
    flexDirection: "column",
    alignItems: 'center',
    width: window.screen.width,
    height: window.screen.height - 50
  },
  table: {
    width: "1000px",
  }
});

const ConnexionWithStyles = withStyles(styles)(Connexion)
const ConnexionWithRouter = withRouter(ConnexionWithStyles)
export { ConnexionWithRouter as Connexion }
