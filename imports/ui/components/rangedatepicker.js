import React from 'react'
import DayPicker, { DateUtils } from 'react-day-picker'
import 'react-day-picker/lib/style.css'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'

export class RangeDatePicker extends React.Component {
  static defaultProps = {
    numberOfMonths: 2,
  }

  constructor(props) {
    super(props);
    this.state = {
      show: false,
      from: undefined,
      to: undefined,
    }
  }

  handleDayClick = (day) => {
    const range = DateUtils.addDayToRange(day, this.state);
    console.log('day',day);
    console.log('range',range);
    this.setState(range);
  }

  handleResetClick = () => {
    this.setState({from: undefined, to: undefined})
  }

  handleChange = () => {
    this.setState({show: !this.state.show})
  }

  render() {
    const { from, to } = this.state;
    const modifiers = { start: from, end: to };
    return (
      <div>
        <Button onClick={this.handleChange} >{ this.state.show ? 'Valider' : 'Sélectionner une date' }</Button>
        { this.state.show ?
          <div style={{position: 'absolute'}}>
            <Paper>
            <p>
              {!from && !to && 'choisissez le premier jour.'}
              {from && !to && 'choisissez le dernier jour.'}
              {from &&
                to &&
                `Selected from ${from.toLocaleDateString()} to
                ${to.toLocaleDateString()}`
              }{' '}
              {from &&
                to && (
                  <button className="link" onClick={this.handleResetClick}>
                    Reset
                  </button>
                )
              }
            </p>
            <DayPicker
              numberOfMonths={this.props.numberOfMonths}
              selectedDays={[from, { from, to }]}
              modifiers={modifiers}
              onDayClick={this.handleDayClick}
              />
            </Paper>
          </div> : ''
        }
      </div>
    );
  }
}
