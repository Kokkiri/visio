import React, { Component } from 'react'
import { withTracker } from 'meteor/react-meteor-data'
import { withStyles } from '@material-ui/core/styles'
import { Link, Route, BrowserRouter, Switch } from 'react-router-dom'
import { FormModal, CandidatListComponent, Connexion } from './components'
import { List } from '../api/list.js'
import Button from '@material-ui/core/Button'

import AccountsUIWrapper from './AccountsUIWrapper.js'

class Selection extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpenModal: false,
      disable: true,
      title: "",
      route: "",
      cellTitle3: "",
      initModalList: [],
      searchInput: "",
      candidatList: [],
      nomCandidat: "",
      jury: "",
      cExamen: "",
    }
  }

  render() {
    const { isOpenModal, title, route, cellTitle3, candidatList, disable, searchInput, nomCandidat, jury, cExamen, roomName} = this.state
    const { classes } = this.props
    const List = ({match}) => {
      return (
        <Switch>
          <Route exact path={match.url} >
            <CandidatListComponent
              cellTitle3={cellTitle3}
              candidatList={candidatList}
              clearList={this.handleClose}
              searchInput={searchInput}
              handleSearchBar={this.handleSearchBar}
              filter={this.candidatsFilter}
              linkToVideoConf={(objCandidat, color) =>
                <Button
                  className={color}
                  component={Link}
                  to={`${match.url}/connexion`}
                  onClick={this.candidateByValue.bind(null, objCandidat)}
                  variant="outlined"
                >Connexion
              </Button>
              }
            />
          </Route>
          <Route
            path={match.url + '/connexion'}
            component={() => <Connexion nomCandidat={nomCandidat} cExamen={cExamen} jury={jury} roomName={`${this.roomName()}`} />}
          />
        </Switch>
      )
    }
    return(
      <BrowserRouter>
        <Switch>
          <Route exact path='/'>
            <div className={classes.root}>

              <FormModal
                isOpenModal={isOpenModal}
                close={this.handleClose}
                title={title}
                getValue={this.groupByValue}
                disable={disable}
                setDisable={this.setDisable}
                searchInput={searchInput}
                handleSearchBar={this.handleSearchBar}
                filter={this.modalFilter}
                linkToCandidatesList={
                  <Button
                    component={Link}
                    to={route}
                    onClick={this.handleValidate}
                    disabled={disable}
                    color="primary"
                    variant="outlined"
                  >Valider
                  </Button>
                }
              />
              <div>
                <AccountsUIWrapper />
                <div className={classes.space} ></div>
                { this.props.currentUser ?
                  <div>
                    <Button className={classes.button} color="primary" variant="outlined" id="a" onClick={this.handleOpen} >Jury</Button>
                    <div className={classes.space} ></div>
                    <Button className={classes.button} color="primary" variant="outlined" id="b" onClick={this.handleOpen} >Candidat</Button>
                  </div> : ''
                }
              </div>
            </div>
          </Route>
          <Route path="/jury" component={List} />
          <Route path="/centredexamen" component={List} />
        </Switch>
      </BrowserRouter>
    )
  }

  // (Selection) Charge la liste des candidats correspondant au jury ou au centre d'examen selectionné
  handleOpen = (e) => {
    const id = e.currentTarget.id
    const data = this.props.list
    const tabCE = []
    const tabJ = []
    data.map(el => {
      if (tabCE.indexOf(el.c_examen) === -1) {
        tabCE.push(el.c_examen)
      }
      if (tabJ.indexOf(el.jury) === -1) {
        tabJ.push(el.jury)
      }
    })
    if (id === "a") {
      this.setState({isOpenModal: true, route: "/jury", title: "commissions de jury", initModalList: tabJ, cellTitle3: "centres d'accueil du candidat"})
    }
    if (id === "b") {
      this.setState({isOpenModal: true, route: "/centredexamen", title: "Centres d'Examen", initModalList: tabCE, cellTitle3: "commissions de jury"})
    }
  }

  // (FormModal) récupère un groupe de candidat en fonction de l'indice dans la liste de jury
  groupByValue = (value) => {
    const data = this.props.list
    let tab = []
    for (let i = 0; i < data.length; i++) {
      if (data[i].color === undefined) {
        Meteor.call('list.update', data[i]._id, false)
      }
      if (data[i].jury !== "undefined" && value === data[i].jury) {
        tab.push(data[i])
      }
      if (data[i].c_examen !== "undefined" && value === data[i].c_examen) {
        tab.push(data[i])
      }
      this.setState({candidatList: tab})
    }
  }

  // (FormModal)
  modalFilter = () => {
    let list = this.state.initModalList
    let userInput = this.state.searchInput
    let ele = list.filter(el => {
      let name = ""
      if (userInput !== undefined) {
        name = el.toLowerCase()
      }
      return name.indexOf(userInput.toLowerCase()) !== -1
    })
    return ele
  }

  // (FormModal)
  handleValidate = () => {
    this.setState({isOpenModal: false, disable: true, searchInput: ""})
  }

  // (FormModal) Permet de s'assurer que l'on ne puisse seléctionner qu'un seul élément dans la liste
  setDisable = () => {
    this.setState({disable: false})
  }

  /* (FormModal, CandidatListComponent) Récupère le choix de l'utilisateur
  pour anticiper les éléments à afficher sur la prochaine page */
  handleSearchBar = (e) => {
    let userInput = e.target.value
    this.setState({searchInput: userInput})
  }

  /* (FormModal, CandidatListComponent) permet de fermer le FormModal et de réinitialiser
  la barre de recherche dont la fonction est commune à FormModal et à CandidatListComponent */
  handleClose = () => {
    this.setState({isOpenModal: false, candidatList: [], disable: true, searchInput: ""})
  }

  // (CandidatListComponent) filtre par heure de passage + selon qu'ils sont déjà passé ou non
  candidatsFilter = (searchInput) => {
    const { candidatList }  = this.state
    let userInput = searchInput

    candidatList.sort((a,b) => {
      if ( a.color === false && b.color === true ) {
        return false
      }
      if ( a.color === true && b.color === false ) {
        return true
      }
      if ( a.color === b.color ) {
        return a.heure > b.heure
      }
    })

    let ele = candidatList.filter(el => {
      let date = el.date.slice(0, 10)
      let datenow = new Date().toISOString().slice(0, 10)
      let list = el.candidat.concat(el.heure).concat(el.c_examen).concat(el.jury).concat(date)
      let name = ""
      if (userInput !== undefined ) {
        name = list.toLowerCase()
      }
      return name.indexOf(userInput.toLowerCase()) !== -1
    })
    return ele
  }

  // (CandidatListComponent) modifie la couleur du bouton connexion et retourne les infos du candidat necessaire à la page connexion
  candidateByValue = (objCandidat) => {
    Meteor.call('list.update', objCandidat._id, true)
    const { candidatList } = this.state
    let tab = []
    for (let i in candidatList) {
      if (candidatList[i] === objCandidat) {
        candidatList[i] = {...candidatList[i], color: true}
      }
      tab.push(candidatList[i])
    }
    this.setState({nomCandidat: objCandidat.candidat, jury: objCandidat.jury, cExamen: objCandidat.c_examen, candidatList: tab})
  }

  // (Connexion) retourne l'id du candidat qui deviendra le nom de la chambre (point de rencontre entre jury et candidat)
  roomName = () => {
    let id = ""
    this.props.list.map((el, i) => {
      if (this.state.nomCandidat === el.candidat) {
        id = el._id
      }
    })
    return id
  }

}

const styles = theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '400px'
  },
  space: {
    margin: theme.spacing.unit * 3
  },
  button: {
    width: "300px",
    height: "100px",
    fontSize: "20px"
  }
});

const track =  withTracker(() => {
  Meteor.subscribe('list');

  return {
    list: List.find({}).fetch(),
    currentUser: Meteor.user(),
  };
})(Selection)

const SelectionWithStyles = withStyles(styles)(track)
export {SelectionWithStyles as Selection}
