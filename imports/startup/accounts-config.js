import { Accounts } from 'meteor/std:accounts-ui'

Accounts.config({
  forbidClientAccountCreation: true
});

Accounts.ui.config({
  passwordSignupFields: 'USERNAME_ONLY'
});
