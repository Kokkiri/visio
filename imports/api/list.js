import { check } from 'meteor/check'
import data from '../../data.json'

export const List = new Mongo.Collection("list")

if (Meteor.isServer) {
  Meteor.publish('list', function () {
    if (List.find().count() < 1) {
      data.map(el => {
        List.insert({...el})
      })
    }
    today = new Date
    today.setDate(today.getDate() -1)
    return List.find({
      "date" : {
        $gte: today.toISOString(),
        $lt: "2019-07-00T00:00:00.00Z"
      }
    })
  })
}

Meteor.methods({
  'list.update'(id, color) {
    check(id, String)
    check(color, Boolean)
    List.update( id, { $set: {color: color} } )
  }
})
