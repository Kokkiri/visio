import React from 'react'
import { Meteor } from 'meteor/meteor'
import { render } from 'react-dom'

import '../imports/startup/accounts-config.js'
import { Selection } from '../imports/ui/app.js'

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core'
import purple from '@material-ui/core/colors/purple'


const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#0097a7",
    },
    secondary: {
      main: "#f50057",
      purple: purple['A400'],
    }
  },
});


Meteor.startup(() => {
  render(
    <MuiThemeProvider theme={theme}>
      <Selection/>
    </MuiThemeProvider>,
    document.getElementById("render-target")
  )
})
