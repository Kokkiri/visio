from faker import Faker
import io, json, random
from datetime import datetime


fake = Faker("fr_FR")

nom_Js = []
nom_CEs = []
for i in range(10):
    nom_j = fake.city()
    nom_ce = fake.city()
    nom_Js.append(nom_j)
    nom_CEs.append(nom_ce)

fake_data = []
candidat = []
for j in range(500):

    iso_format = "{Year}-{Month}-{Day}T{Hour}:{Minute}:{Offset}Z"

    t = int('{}{:%m}'.format("", datetime.now()))

    month_range = [str(i).zfill(2) for i in range(t, t + 2)]
    day_range = [str(i).zfill(2) for i in range(1, 31)]
    hour_range = [str(i).zfill(2) for i in range(1, 25)]
    min_range = [str(i).zfill(2) for i in range(1, 60)]

    argz = {"Year": "2019",
            "Month": random.choice(month_range),
            "Day" : random.choice(day_range),
            "Hour": "00",
            "Minute": "00",
            "Offset": "00.00"
            }

    date = iso_format.format(**argz)
    nom = fake.name()
    heure = fake.time(pattern="%H:%M")
    # je recupere aleatoirement une des valeurs de nomJs et nomCEs
    nom_j = random.choice(nom_Js)
    nom_ce = random.choice(nom_CEs)
    fake_data.append({"date": date, "jury": nom_j, "c_examen": nom_ce, "candidat": nom, "heure": heure})


with io.open('./data.json', 'w', encoding='utf-8') as f:
    f.write(json.dumps(fake_data, ensure_ascii=False, sort_keys=True, indent=4))

# print(fake_data)
